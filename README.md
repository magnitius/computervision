# SJTU Conputer Vision Group 10 #

## Creared by: ##
*  Anton Voronov
*  Sidra Aleem
*  Husein Sulianto
*  Ruth-Emely Pierau
*  Muhammad Adnan Mumtaz

### This repo is for representing analysis of diffrent techniques to improve the quality of an image for face recognition ###

## Main goals: ##
* Investigate how image preprocessing effect to face recognition in sevelar preprocessing methods:

1. Bilinear filter
2. Gaussian filter
3. Integral image preprocessing
4. Median filter
5. Bilateral filter
6. Fourier transformation

* Check perfomance of each filter
* Use real pictures from Social Networks to train recognition model

### Setup detailes ###

## This project use Python 2 (recommendet to use Anaconda2 Pack), with next librares: ##

* PyQt5 for GUI interface
* numpy, matplotlib and cv2 (opencv library, recommendet to use v.3 and higher)
* requests module is also requied for dowloading data from VK.COM
* standart libraries: sys, os, threading

### Who do I talk to? ###

you can contact to the repo administrator: magnitius - magnitius@sjtu.edu.cn