# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'CV_GUI.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPixmap


class Ui_MainWindow(object):

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(488, 605)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(488, 605))
        MainWindow.setMaximumSize(QtCore.QSize(488, 605))
        MainWindow.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.inputPhotoView = QtWidgets.QGraphicsView(self.centralwidget)
        self.inputPhotoView.setGeometry(QtCore.QRect(20, 90, 141, 171))
        self.inputPhotoView.viewport().setProperty("cursor", QtGui.QCursor(QtCore.Qt.ForbiddenCursor))
        self.inputPhotoView.setObjectName("inputPhotoView")
        self.image_filter_bilinear = QtWidgets.QGraphicsView(self.centralwidget)
        self.image_filter_bilinear.setGeometry(QtCore.QRect(20, 300, 100, 100))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(100)
        sizePolicy.setVerticalStretch(100)
        sizePolicy.setHeightForWidth(self.image_filter_bilinear.sizePolicy().hasHeightForWidth())
        self.image_filter_bilinear.setSizePolicy(sizePolicy)
        self.image_filter_bilinear.setMinimumSize(QtCore.QSize(100, 100))
        self.image_filter_bilinear.setMaximumSize(QtCore.QSize(100, 100))
        self.image_filter_bilinear.setObjectName("image_filter_bilinear")
        self.image_filter_median = QtWidgets.QGraphicsView(self.centralwidget)
        self.image_filter_median.setGeometry(QtCore.QRect(20, 450, 100, 100))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(100)
        sizePolicy.setVerticalStretch(100)
        sizePolicy.setHeightForWidth(self.image_filter_median.sizePolicy().hasHeightForWidth())
        self.image_filter_median.setSizePolicy(sizePolicy)
        self.image_filter_median.setMinimumSize(QtCore.QSize(100, 100))
        self.image_filter_median.setMaximumSize(QtCore.QSize(100, 100))
        self.image_filter_median.setObjectName("image_filter_median")
        self.image_filter_gaussian = QtWidgets.QGraphicsView(self.centralwidget)
        self.image_filter_gaussian.setGeometry(QtCore.QRect(160, 300, 100, 100))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(100)
        sizePolicy.setVerticalStretch(100)
        sizePolicy.setHeightForWidth(self.image_filter_gaussian.sizePolicy().hasHeightForWidth())
        self.image_filter_gaussian.setSizePolicy(sizePolicy)
        self.image_filter_gaussian.setMinimumSize(QtCore.QSize(100, 100))
        self.image_filter_gaussian.setMaximumSize(QtCore.QSize(100, 100))
        self.image_filter_gaussian.setObjectName("image_filter_gaussian")
        self.image_filter_bilateral = QtWidgets.QGraphicsView(self.centralwidget)
        self.image_filter_bilateral.setGeometry(QtCore.QRect(160, 450, 100, 100))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(100)
        sizePolicy.setVerticalStretch(100)
        sizePolicy.setHeightForWidth(self.image_filter_bilateral.sizePolicy().hasHeightForWidth())
        self.image_filter_bilateral.setSizePolicy(sizePolicy)
        self.image_filter_bilateral.setMinimumSize(QtCore.QSize(100, 100))
        self.image_filter_bilateral.setMaximumSize(QtCore.QSize(100, 100))
        self.image_filter_bilateral.setObjectName("image_filter_bilateral")
        self.image_filter_integral = QtWidgets.QGraphicsView(self.centralwidget)
        self.image_filter_integral.setGeometry(QtCore.QRect(300, 300, 100, 100))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(100)
        sizePolicy.setVerticalStretch(100)
        sizePolicy.setHeightForWidth(self.image_filter_integral.sizePolicy().hasHeightForWidth())
        self.image_filter_integral.setSizePolicy(sizePolicy)
        self.image_filter_integral.setMinimumSize(QtCore.QSize(100, 100))
        self.image_filter_integral.setMaximumSize(QtCore.QSize(100, 100))
        self.image_filter_integral.setObjectName("image_filter_integral")
        self.image_filter_fourier = QtWidgets.QGraphicsView(self.centralwidget)
        self.image_filter_fourier.setGeometry(QtCore.QRect(300, 450, 100, 100))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(100)
        sizePolicy.setVerticalStretch(100)
        sizePolicy.setHeightForWidth(self.image_filter_fourier.sizePolicy().hasHeightForWidth())
        self.image_filter_fourier.setSizePolicy(sizePolicy)
        self.image_filter_fourier.setMinimumSize(QtCore.QSize(100, 100))
        self.image_filter_fourier.setMaximumSize(QtCore.QSize(100, 100))
        self.image_filter_fourier.setObjectName("image_filter_fourier")
        self.button_select_path = QtWidgets.QPushButton(self.centralwidget)
        self.button_select_path.setGeometry(QtCore.QRect(170, 90, 101, 21))
        self.button_select_path.setObjectName("button_select_path")
        self.label_image_filter_bilinear = QtWidgets.QLabel(self.centralwidget)
        self.label_image_filter_bilinear.setGeometry(QtCore.QRect(40, 280, 54, 12))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.label_image_filter_bilinear.setFont(font)
        self.label_image_filter_bilinear.setObjectName("label_image_filter_bilinear")
        self.label_image_filter_gaussian = QtWidgets.QLabel(self.centralwidget)
        self.label_image_filter_gaussian.setGeometry(QtCore.QRect(180, 280, 54, 12))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.label_image_filter_gaussian.setFont(font)
        self.label_image_filter_gaussian.setObjectName("label_image_filter_gaussian")
        self.label_image_filter_integral = QtWidgets.QLabel(self.centralwidget)
        self.label_image_filter_integral.setGeometry(QtCore.QRect(320, 280, 54, 12))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.label_image_filter_integral.setFont(font)
        self.label_image_filter_integral.setObjectName("label_image_filter_integral")
        self.label_image_filter_median = QtWidgets.QLabel(self.centralwidget)
        self.label_image_filter_median.setGeometry(QtCore.QRect(50, 430, 54, 12))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.label_image_filter_median.setFont(font)
        self.label_image_filter_median.setObjectName("label_image_filter_median")
        self.label_image_filter_bilateral = QtWidgets.QLabel(self.centralwidget)
        self.label_image_filter_bilateral.setGeometry(QtCore.QRect(180, 430, 54, 12))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.label_image_filter_bilateral.setFont(font)
        self.label_image_filter_bilateral.setObjectName("label_image_filter_bilateral")
        self.label_image_filter_fourier = QtWidgets.QLabel(self.centralwidget)
        self.label_image_filter_fourier.setGeometry(QtCore.QRect(320, 430, 54, 12))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.label_image_filter_fourier.setFont(font)
        self.label_image_filter_fourier.setObjectName("label_image_filter_fourier")
        self.label_input_image = QtWidgets.QLabel(self.centralwidget)
        self.label_input_image.setGeometry(QtCore.QRect(40, 60, 91, 20))
        self.label_input_image.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_input_image.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label_input_image.setMidLineWidth(1)
        self.label_input_image.setTextFormat(QtCore.Qt.PlainText)
        self.label_input_image.setScaledContents(False)
        self.label_input_image.setAlignment(QtCore.Qt.AlignCenter)
        self.label_input_image.setObjectName("label_input_image")
        self.edit_path_line = QtWidgets.QLineEdit(self.centralwidget)
        self.edit_path_line.setGeometry(QtCore.QRect(280, 90, 191, 20))
        self.edit_path_line.setObjectName("edit_path_line")
        self.image_original = QtWidgets.QGraphicsView(self.centralwidget)
        self.image_original.setGeometry(QtCore.QRect(200, 150, 100, 100))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(100)
        sizePolicy.setVerticalStretch(100)
        sizePolicy.setHeightForWidth(self.image_original.sizePolicy().hasHeightForWidth())
        self.image_original.setSizePolicy(sizePolicy)
        self.image_original.setMinimumSize(QtCore.QSize(100, 100))
        self.image_original.setMaximumSize(QtCore.QSize(100, 100))
        self.image_original.setObjectName("image_original")
        self.label_image_cut = QtWidgets.QLabel(self.centralwidget)
        self.label_image_cut.setGeometry(QtCore.QRect(220, 130, 54, 12))
        self.label_image_cut.setTextFormat(QtCore.Qt.PlainText)
        self.label_image_cut.setObjectName("label_image_cut")
        self.label_input_image_path = QtWidgets.QLabel(self.centralwidget)
        self.label_input_image_path.setGeometry(QtCore.QRect(260, 70, 101, 20))
        self.label_input_image_path.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_input_image_path.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label_input_image_path.setMidLineWidth(1)
        self.label_input_image_path.setTextFormat(QtCore.Qt.PlainText)
        self.label_input_image_path.setScaledContents(False)
        self.label_input_image_path.setAlignment(QtCore.Qt.AlignCenter)
        self.label_input_image_path.setObjectName("label_input_image_path")
        self.label_tittle = QtWidgets.QLabel(self.centralwidget)
        self.label_tittle.setGeometry(QtCore.QRect(23, 10, 451, 41))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_tittle.setFont(font)
        self.label_tittle.setTextFormat(QtCore.Qt.PlainText)
        self.label_tittle.setAlignment(QtCore.Qt.AlignCenter)
        self.label_tittle.setWordWrap(True)
        self.label_tittle.setObjectName("label_tittle")
        self.result_bilinear = QtWidgets.QLabel(self.centralwidget)
        self.result_bilinear.setGeometry(QtCore.QRect(40, 410, 54, 12))
        self.result_bilinear.setObjectName("result_bilinear")
        self.result_bilinear_2 = QtWidgets.QLabel(self.centralwidget)
        self.result_bilinear_2.setGeometry(QtCore.QRect(180, 410, 54, 12))
        self.result_bilinear_2.setObjectName("result_bilinear_2")
        self.result_bilinear_3 = QtWidgets.QLabel(self.centralwidget)
        self.result_bilinear_3.setGeometry(QtCore.QRect(320, 410, 54, 12))
        self.result_bilinear_3.setObjectName("result_bilinear_3")
        self.result_bilinear_4 = QtWidgets.QLabel(self.centralwidget)
        self.result_bilinear_4.setGeometry(QtCore.QRect(320, 560, 54, 12))
        self.result_bilinear_4.setObjectName("result_bilinear_4")
        self.result_bilinear_5 = QtWidgets.QLabel(self.centralwidget)
        self.result_bilinear_5.setGeometry(QtCore.QRect(40, 560, 54, 12))
        self.result_bilinear_5.setObjectName("result_bilinear_5")
        self.result_bilinear_6 = QtWidgets.QLabel(self.centralwidget)
        self.result_bilinear_6.setGeometry(QtCore.QRect(180, 560, 54, 12))
        self.result_bilinear_6.setObjectName("result_bilinear_6")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.button_select_path.clicked.connect(self.select_path)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.button_select_path.setText(_translate("MainWindow", "Select path..."))
        self.label_image_filter_bilinear.setText(_translate("MainWindow", "Bilinear"))
        self.label_image_filter_gaussian.setText(_translate("MainWindow", "Gaussian"))
        self.label_image_filter_integral.setText(_translate("MainWindow", "Integral"))
        self.label_image_filter_median.setText(_translate("MainWindow", "Median"))
        self.label_image_filter_bilateral.setText(_translate("MainWindow", "Bilateral"))
        self.label_image_filter_fourier.setText(_translate("MainWindow", "Fourier"))
        self.label_input_image.setText(_translate("MainWindow", "Input Image"))
        self.label_image_cut.setText(_translate("MainWindow", "Original"))
        self.label_input_image_path.setText(_translate("MainWindow", "Image Path"))
        self.label_tittle.setText(_translate("MainWindow", "Group 1 Computer Vision project: Analysis of filters impact to the face recognition"))
        self.result_bilinear.setText(_translate("MainWindow", "No results"))
        self.result_bilinear_2.setText(_translate("MainWindow", "No results"))
        self.result_bilinear_3.setText(_translate("MainWindow", "No results"))
        self.result_bilinear_4.setText(_translate("MainWindow", "No results"))
        self.result_bilinear_5.setText(_translate("MainWindow", "No results"))
        self.result_bilinear_6.setText(_translate("MainWindow", "No results"))


    def select_path(self):
        filename = QtWidgets.QFileDialog.getOpenFileName()[0]
        self.edit_path_line.setText(filename)
        print("Gui: Start operation script")
        print("Gui: original file path: " + filename)
        self.set_input_image(filename)
        print("Gui: finished to set up")


    def set_input_image(self, filepath):
        print("Log_original: Start to set original image")
        self.sence = QtWidgets.QGraphicsScene()
        self.sence.addPixmap(QPixmap(filepath))
        self.inputPhotoView.setScene(self.sence)
        # self.inputPhotoView.show()
        print("Log_original: Image added")


    def set_original_image(self, filepath):
        print("Log_original: Start to set original image")
        self.sence = QtWidgets.QGraphicsScene()
        # new_filepath = //call Husein function
        # if not (new_filepath == "bad")
        self.sence.addPixmap(QPixmap(filepath))
        self.image_original


    def set_bilinear_result(self, filepath):
        pass


    def set_gaussian_result(self, filepath):
        pass


    def set_integral_result(self, filepath):
        pass


    def set_median_result(self, filepath):
        pass


    def set_bilateral_result(self, filepath):
        pass


    def set_fourier_result(self, filepath):
        pass


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())


