import requests
import os
import threading
#from PyQt5 import QtGui,QtXml,QtCore,uic

# token = '4ab379c5903fe676fe648e0cf63чм370dc64aa60713bcde3bd98c9605ebd5ae968495a432a63f3b7f5f8f28'
import time

account_list = []
API_URL = 'http://api.vk.com/method/'
VK_BAD_PHOTO = "https://vk.com/images/deactivated_200.png"
lock = threading.Lock()
VK_DEFAULT_PHOTO = "https://vk.com/images/camera_200.png"
default_parcer_number = int(87000)

def download(url, file_name):
    print("downloading: " + url)
    with open(file_name, "wb") as file:
        response = requests.get(url)
        file.write(response.content)
        print("download complete, file saved as " + file_name)


def initiate():
    lock.acquire()
    try:
        curren_max_value = len(account_list)
        if curren_max_value == 0:
            for i in range(50):
                account_list.append(i+default_parcer_number)
        else:
            for i in range(50):
                account_list[i] += 50
        print("Formed query set from id:" + str(account_list[0]) + " to id:" + str(account_list[49]))
    finally:
        lock.release()


def multi_user_getter():
    request_string = ""
    for i in account_list:
        request_string += str(i) + ","
    print("Query list formed: " + request_string)
    return request_string


def parsing_by_request():
    initiate()
    response = requests.get(API_URL + 'users.get', {'user_ids': multi_user_getter(), 'fields': 'sex,photo_200_orig'})
    result = response.text
    import json
    response = json.loads(result)
    users = response['response']
    # sex: 1->female, 2->male, 0->do not know
    profiles = [{'sex': 1, 'photo_200_orig': "https://vk.com/images/deactivated_200.png", 'uid': 0}]
    for user in users:
        profiles.append({'sex': user['sex'], 'photo_200_orig': user['photo_200_orig'], 'uid': user['uid']})
    if not os.path.exists("files/" + '0/'):
        os.makedirs("files/" + '0/')
    if not os.path.exists("Files/" + '1/'):
        os.makedirs("files/" + '1/')
    if not os.path.exists("Files/" + '2/'):
        os.makedirs("files/" + '2/')
    for itr in profiles:
        ups = []
        tilt = 0
        if not (itr['photo_200_orig'] == VK_BAD_PHOTO or itr['photo_200_orig'] == VK_DEFAULT_PHOTO):
            file_name = "files/" + str(itr['sex']) + "/" + str(itr['uid']) + ".jpg"
            sorce = itr['photo_200_orig']
            ups.append(threading.Thread(target=download, args=(sorce, file_name)).start())


def start_parsing(max_iteration_time, max_grab_threats):
    sleep_time = 4
    for walk in range(max_iteration_time):
        threads_array = []
        for step in range(max_grab_threats):
            # initiate()
            threads_array.append(threading.Thread(target=parsing_by_request(), args=()).start())
        print("Sleep before iteration: " + str(sleep_time) + " seconds")
        time.sleep(sleep_time)
        print("Back to fight!")



# Start program from here!
print("Welcome to VK BigData Photo parser! We are going to create 3 folders")
print("Folder \"1\" is for female photos")
print("Folder \"2\" is for male photos")
print("Folder \"0\" is for photos, where we do know know users\'s gender")

iterations = int(input("Input maximum iteration times: "))
threads_to_walk = int(input("Input maximum threads in same time for parsing: "))
start_parsing(iterations, threads_to_walk)
