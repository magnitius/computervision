from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import os, sys
import CV_GUI


def check_dir(directory): # check and create directories
    if not os.path.exists(directory):
        os.makedirs(directory)

# initiate directories
if (os.path.exists(os.getcwd() + "/result_files")):
    if not (os.path.exists(os.getcwd() + "/old_results")):
        newdir = os.getcwd() + "/old_results" + "/result_files" + str(qrand())
        os.makedirs(newdir)
        os.makedirs(os.getcwd() + "/old_results")
        os.replace(os.getcwd() + "/result_files", os.getcwd() + "/old_results")
dir_defaults = os.getcwd() + "/defaults"
dir_result_files = os.getcwd() + "/result_files"
check_dir(dir_result_files)
dir_result_bilinear = dir_result_files + "/bilinear"
check_dir(dir_result_bilinear)
dir_result_gaussian = dir_result_files + "/gaussian"
check_dir(dir_result_gaussian)
dir_result_integral = dir_result_files + "/integral"
check_dir(dir_result_integral)
dir_result_median = dir_result_files + "/median"
check_dir(dir_result_median)
dir_result_bilateral = dir_result_files + "/bilateral"
check_dir(dir_result_bilateral)
dir_result_fourier = dir_result_files + "/fourier"
check_dir(dir_result_fourier)


# create application window

app = QApplication(sys.argv)
window = QMainWindow()
ui = CV_GUI.Ui_MainWindow()
ui.setupUi(window)


window.show()
sys.exit(app.exec_())

